CONTENTS OF THIS FILE
---------------------

 * Introduction
 * How it works
 * Installation
 * Similar modules
 * Configuration
 * References

INTRODUCTION
------------

You want to add Google Analytics tracking to your website but do no want to use
the GA cookie which comes along ? You have a simple site, and you don't want to
display the annoying cookie banner just for Analytics tracking ?

This module allows you to collect statistics about your visitors via Google
Analytics without having to set a cookie on your website.

HOW IT WORKS
------------

When calling the Google Analytics code snippet, this module deactivates the
cookie storage and generate a user ID by using a browser Fingerprint with the
[Fingerprintjs2 library](https://github.com/Valve/fingerprintjs2):

```js
Fingerprint2.get(function (components) {
    var values = components.map(function (component) {
        return component.value;
    });
    var murmur = Fingerprint2.x64hash128(values.join(''), 31);
    ga('create', drupalSettings.google_analytics_cookieless.ga_account, {
        'storage': 'none',
        'clientId': murmur
    });
    ga('set', 'anonymizeIp', drupalSettings.google_analytics_cookieless.ga_anonymise_ip);
    ga('send', 'pageview');
})
```

INSTALLATION
------------

The installation of this module requires the eternal library FingerPrintJS.

First enable the Drupal module like any other modules.

 1. Copy/upload the google_analytics_cookieless module to the modules directory
of your Drupal installation.

 2. Enable the 'Google Analytics Cookieless' module in 'Extend'. 
   (/admin/modules)

 3.  You can use composer to download and install the FingerprintJS library. 
     
 a. For this you have to add to your composer.json file the following
 repository:

```
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
          "type": "package",
          "package": {
            "name": "valve/fingerprintjs2",
            "version": "2.1.0",
            "type": "drupal-library",
            "extra": {
              "installer-name": "fingerprintjs"
            },
            "source": {
              "url": "https://github.com/Valve/fingerprintjs2",
              "type": "git",
              "reference": "2.1.0"
            }
          }
        }
```

 b. Then add the library as a required composer package :

`composer require --prefer-dist valve/fingerprintjs2`

 Otherwise (not recommended) you could download the library from
 https://github.com/Valve/fingerprintjs2
 and place the following file fingerprint2.js under the libraries/fingerprintjs
 folder.
    
 c. The complete path of the file should be :
 libraries/fingerprint/fingerprint2.js
 
SIMILAR MODULES
---------------
This module could probably be merged with the main Google Analytics module as
some code / features are the same :
[Google Analytics](https://www.drupal.org/project/google_analytics)

CONFIGURATION
-------------

 * FingerprintJS library does not come with a minified version by default. It
is possible to use a CDN version or to build a compiled one with npm/yarn.
 See : https://github.com/Valve/fingerprintjs2/issues/504
 
On a Drupal website, you can just enable JS aggregation on the performance page 
(admin/config/development/performance).

REFERENCES
----------

https://j2h.com/blog/google-analytics-without-cookies
https://github.com/f055/cookieless-google-analytics (using fingerprint v1)
