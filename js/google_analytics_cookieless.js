/**
 * @file
 * google_analytics_cookieless.js.
 */

(function ($, Drupal, drupalSettings) {
    'use strict';

    Drupal.behaviors.google_analytics_cookieless = {
        attach: function (context, settings) {
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', drupalSettings.google_analytics_cookieless.ga_js_file_address, 'ga');

            if (window.requestIdleCallback) {
                requestIdleCallback(function () {
                    addGoogleAnalytics();
                })
            } else {
                setTimeout(function () {
                    addGoogleAnalytics();
                }, 500)
            }

            /**
             * Instanciate GA object with a user ID by using a browser Fingerprint.
             */
            function addGoogleAnalytics() {
                Fingerprint2.get(function (components) {
                    var values = components.map(function (component) {
                        return component.value;
                    });
                    var murmur = Fingerprint2.x64hash128(values.join(''), 31);
                    ga('create', drupalSettings.google_analytics_cookieless.ga_account, {
                        'storage': 'none',
                        'clientId': murmur
                    });
                    ga('set', 'anonymizeIp', drupalSettings.google_analytics_cookieless.ga_anonymise_ip);
                    ga('send', 'pageview');
                })
            }
        }
    };

})(jQuery, Drupal, drupalSettings);
