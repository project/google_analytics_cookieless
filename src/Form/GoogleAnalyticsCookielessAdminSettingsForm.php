<?php

namespace Drupal\google_analytics_cookieless\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The GoogleAnalyticsCookielessAdminSettingsForm Class.
 */
class GoogleAnalyticsCookielessAdminSettingsForm extends ConfigFormBase {

  /**
   * A module handler.
   *
   * @var Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The current user.
   *
   * @var Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $currentUser, ModuleHandler $moduleHandler) {
    parent::__construct($config_factory);
    $this->currentUser = $currentUser;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['google_analytics_cookieless.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_analytics_cookieless_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('google_analytics_cookieless.settings');

    $form['google_analytics_cookieless_account'] = [
      '#default_value' => $config->get('account'),
      '#description' => $this->t('This ID is unique to each site you want to track separately, and is in the form of UA-xxxxxxx-yy. To get a Web Property ID, <a target="_blank" href=":analytics">register your site with Google Analytics</a>, or if you already have registered your site, go to your Google Analytics Settings page to see the ID next to every site profile. <a target="_blank" href=":webpropertyid">Find more information in the documentation</a>.', [
        ':analytics' => 'https://marketingplatform.google.com/about/analytics/',
        ':webpropertyid' => Url::fromUri('https://developers.google.com/analytics/resources/concepts/gaConceptsAccounts', ['fragment' => 'webProperty'])
          ->toString(),
      ]),
      '#maxlength' => 20,
      '#placeholder' => 'UA-',
      '#required' => TRUE,
      '#size' => 20,
      '#title' => $this->t('Web Property ID'),
      '#type' => 'textfield',
    ];

    $form['google_analytics_cookieless_track_logged_in_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track logged-in Drupal users'),
      '#description' => $this->t('Send statistics about connected users. If connected Drupal users are only administrators or content contributors you might want to no track them.'),
      '#default_value' => $config->get('track_logged_in_users'),
    ];

    $form['google_analytics_cookieless_anonymise_ip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Anonymise visitors IP address'),
      '#description' => $this->t('Anonymise visitors IP address sent to Google Analytics (recommended for privacy reason).'),
      '#default_value' => $config->get('anonymise_ip'),
    ];

    $form['google_analytics_cookieless_js_file_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GA Javascript file address'),
      '#description' => $this->t('Address of the Google Analytics Javascript file. You can keep the default value (https://www.google-analytics.com/analytics.js) unless you host your own file somewhere else.'),
      '#default_value' => $config->get('js_file_address'),
    ];

    $visibility_request_path_pages = $config->get('request_path_pages');

    $options = [
      $this->t('Every page except the listed pages'),
      $this->t('The listed pages only'),
    ];

    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
      '%blog' => '/blog',
      '%blog-wildcard' => '/blog/*',
      '%front' => '<front>',
    ]);

    $title = $this->t('Pages');

    $form['google_analytics_cookieless_visibility_request_path_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $config->get('request_path_mode'),
    ];
    $form['google_analytics_cookieless_visibility_request_path_pages'] = [
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => !empty($visibility_request_path_pages) ? $visibility_request_path_pages : '',
      '#description' => $description,
      '#rows' => 10,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match('/^UA-\d+-\d+$/', $form_state->getValue('google_analytics_cookieless_account'))) {
      $form_state->setErrorByName('google_analytics_cookieless_account', $this->t('A valid Google Analytics Web Property ID is case sensitive and formatted like UA-xxxxxxx-yy.'));
    }
    if (filter_var($form_state->getValue('google_analytics_cookieless_js_file_address'), FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setErrorByName('google_analytics_cookieless_js_file_address', $this->t('A valid Google Analytics address is required (default : https://www.google-analytics.com/analytics.js) .'));
    }

    // Verify that every path is prefixed with a slash, but don't check PHP
    // code snippets and do not check for slashes if no paths configured.
    if (!empty($form_state->getValue('google_analytics_cookieless_visibility_request_path_pages'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('google_analytics_cookieless_visibility_request_path_pages'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName('google_analytics_cookieless_visibility_request_path_pages', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('google_analytics_cookieless.settings');
    $config
      ->set('account', $form_state->getValue('google_analytics_cookieless_account'))
      ->set('track_logged_in_users', $form_state->getValue('google_analytics_cookieless_track_logged_in_users'))
      ->set('anonymise_ip', $form_state->getValue('google_analytics_cookieless_anonymise_ip'))
      ->set('js_file_address', $form_state->getValue('google_analytics_cookieless_js_file_address'))
      ->set('request_path_mode', $form_state->getValue('google_analytics_cookieless_visibility_request_path_mode'))
      ->set('request_path_pages', $form_state->getValue('google_analytics_cookieless_visibility_request_path_pages'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
